#!/usr/bin/env python

import rospy
import numpy as np
from sensor_msgs.msg import JointState
from hand_services.srv import *
class inHandClient(object):
    def __init__(self):
        self.rr_toggle_srv="/rr/control_toggle"
        self.update_rr_srv="/rr/update_params"

    def update_rr_params(self,fingers,q0,rigid_f=3):
        rospy.wait_for_service(self.update_rr_srv)
        q_init=JointState()
        q_init.position=q0
        try:
            req=rospy.ServiceProxy(self.update_rr_srv,updateRelaxedRigidityParams)
            resp=req.call(fingers,rigid_f,[],q_init)
            return resp.success
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def toggle_control(self,srv_name,control_status):
        rospy.wait_for_service(srv_name)
        try:
            req=rospy.ServiceProxy(srv_name,ToggleController)
            resp=req.call(control_status)
            return resp.status
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
        
    def enable_rr_control(self):
        self.toggle_control(self.rr_toggle_srv,True)
    def disable_rr_control(self):
        self.toggle_control(self.rr_toggle_srv,False)

'''
if __name__=='__main__':
    rospy.init_node("test")
    hand_cl=inHandClient()

    fingers=[0,1,3]
    des_js=np.array([0.2,0.7,0.8,0.1,
                     0.2,0.7,0.8,0.1,
                     0.0,0.0,0.0,0.0,
                     1.3,0.0,0.7,0.0])

    hand_cl.update_rr_params(fingers,des_js)

    raw_input('run control?')
    hand_cl.enable_rr_control()

    raw_input('stop control?')
    hand_cl.disable_rr_control()
'''
