/*
 * Copyright (C) 2018  Balakumar Sundaralingam, LL4MA Lab, University of Utah
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <allegro_robot_interface/allegro_interface.h>
#include <ll4ma_robot_control/control.h>
#include <geometry_msgs/WrenchStamped.h>
#include <biotac_sensors/BioTacForce.h>
#include <hand_services/SendHandCommand.h>
#include <biotac_sensors/UpdateTare.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>

namespace hand_services
{
namespace control_interface
{
/// Class for interfacing with a robot hand assuming a low level controller runs PD for position and direct torque control
class controlInterface: public Controller
{
public:
  /** 
   * Class initializer
   * 
   * @param ns namespace for the nodehandle
   * @param robot_interface hand_interface pointer
   * @param loop_rate control loop rate
   * 
   * @return None (class initializer)
   */
  controlInterface(std::string ns, robot_interface::RobotInterface *robot_interface,double loop_rate): Controller(ns,robot_interface, loop_rate){ }
  
  bool configureHook();
  bool startHook();
  void updateHook();
  void stopHook();
  void cleanupHook();

  /** 
   * computes the norm-2 squared error between desired joint positions and current joint positions
   * 
   * @param des_js desired joint positions
   * 
   * @return true if within #goal_thresh_
   */
  bool computeError(const Eigen::VectorXd &des_js, double &err);

  /** 
   * Checks if a fingertip is in contact
   * 
   * @param f_idx 
   * 
   * @return  true if greater that #contact_prob_
   */
  bool checkContact(int f_idx);

  /** 
   * sends a time interpolated joint position command between current joint position and  desired joint position.
   * 
   * @param des_js desired joint position to which the joints need to move.
   * 
   * @return true when goal is reached.
   */
  bool sendDesJPos(const Eigen::VectorXd &des_js);

  void externalCmdCallback(sensor_msgs::JointState msg);

protected:
  void log(std::string msg);
  void log(std::string msg, LogLevel level);

  /// fingers to check for contact. TODO: also for goal reached.
  std::vector<int> f_idx_;

  /// contact probability threshold
  double contact_prob_;
  
  /// tactile pressure data subscriber for tactile feedback control.
  ros::Subscriber tactile_sub_;

  /// External controller subscriber:
  ros::Subscriber external_cmd_sub_;
  /// tactile pressure topic string, loaded from rosparam
  std::string tactile_sub_topic_;

  /// tactile topics for different fingers, provided by simulation (Gazebo), loaded from rosparam, not useful outside the simulation
  std::string tactile_index_topic_;
  std::string tactile_middle_topic_;
  std::string tactile_ring_topic_;
  std::string tactile_thumb_topic_;

  /** 
   *  subscriber callback function that listens to contact topic
   * 
   * @param msg is an array of int with 1=contact and 0=no contact
   */
  void tactileCallback(const biotac_sensors::BioTacForce::ConstPtr& msg);

  /** 
   *  subscriber callback function that listens to gazebo's topics for biotac sensors
   * 
   * @param msg is WrenchStamped format, it contains force and torque xyz values
   */

  void tactileCallbackSimulation(const geometry_msgs::WrenchStamped::ConstPtr& msg);
  void indexTactileCallbackSimulation(const geometry_msgs::WrenchStamped::ConstPtr& msg);
  void middleTactileCallbackSimulation(const geometry_msgs::WrenchStamped::ConstPtr& msg);
  void ringTactileCallbackSimulation(const geometry_msgs::WrenchStamped::ConstPtr& msg);
  void thumbTactileCallbackSimulation(const geometry_msgs::WrenchStamped::ConstPtr& msg);
  ros::Subscriber tactile_sub_i_;
  ros::Subscriber tactile_sub_m_;
  ros::Subscriber tactile_sub_r_;
  ros::Subscriber tactile_sub_t_;

  Eigen::VectorXd ext_j_cmd,ext_jdot_cmd;
  // flag to check for new data from tactile sensor
  bool tactile_state_received_;

  // data from tactile sensor callback( this is a contact probability array)
  Eigen::VectorXd tactile_data_;

  // array of forces (x,y,z) read from tactile sensor
  Eigen::VectorXd tactile_force_data_;
  
  // Receives robot command request
  ros::ServiceServer control_req_;

  /// tactile sensor reset service client 
  ros::ServiceClient tare_reset_srv_;

  /// tare reset serivce name
  std::string tare_reset_name_;
  
  /// hand command service request name
  std::string hand_cmd_srv_;

  bool update_virtual_frame_;
  /// Virtual frame for stiffness control
  Eigen::Vector3d virtual_obj_pose_;
  /** 
   * hand command request callback function
   * 
   * @param req service request data
   * @param resp service response data
   */
  bool handCommand(hand_services::SendHandCommand::Request &req, hand_services::SendHandCommand::Response &resp);

  /** 
   * sends a joint position trajectory to the robot
   * 
   * @param jtraj desired joint position trajectory
   * @param contact_sensing if true will stop moving fingers if contact on each fingertip is detected, requires #tactile_enabled_ to be true
   * 
   * @return 
   */
  bool sendTrajectory(trajectory_msgs::JointTrajectory &jtraj, bool contact_sensing, const std::vector<int> &fingers);

  /** 
   * update the virtual frame used for grasp impedance/stiffness control
   * 
   * @param f_idx fingertips to use for frame computation
   * 
   * @return true once setup
   */
  //bool updateVirtualFrame(const std::vector<int> &f_idx);
  
  /** 
   * stiffness control by using virtual frame on a grasped object, only works when #tactile_enabled_ is true
   * 
   * @param f_idx fingers to use with stiffness control.
   * 
   * @return 
   */
  //bool stiffnessControl(const std::vector<int> &f_idx, Eigen::VectorXd &j_cmd);

  /** 
   * resets the tare offsets in the tactile pressure sensor driver to reduce sensor drift
   * 
   * @param f_idx sensor indices to reset.
   * 
   * @return true on successful reset.
   */
  bool resetTactileTare(const std::vector<int> &f_idx);
  
  /// stiffness controller gains
  Eigen::VectorXd kp_,kd_;

  /// The joint command that was sent to the robot that enabled contact of the fingertips.
  Eigen::VectorXd contact_des_jcmd_;
  /// Threshold to check if a joint trajectory/joint position command has moved the hand to the desired joint state.
  double goal_thresh_;

  /// time in seconds to run the command until the service call is returned (in case goal is not reached)
  double cmd_timeout_;
  
  /// flag to toggle tactile sensor feedback
  bool tactile_enabled_;

  /// flag to know if tactile data is simulated or real
  bool simulation_;

  /// flags to store first contact detection state.
  std::vector<bool> first_contact_;

  /// Flag to enable external controller:
  bool external_control_;
  /// Flag to enable/disable stiffness control
  bool stiffness_control_;

  Eigen::VectorXd stiff_jcmd_;//=q_;

  /// fingers to use for stiffness control
  std::vector<int> stiffness_control_fingers_;

  std::vector<Eigen::Vector3d> force_dirs_;

  /** 
   * Relaxed rigidity control to maintain grasp of object during object manipulation
   * 
   * @param f_idx 
   * @param j_cmd 
   * 
   * @return 
   */
  //bool relaxedRigidityControl(const std::vector<int> &f_idx, Eigen::VectorXd &j_cmd);
  
  //bool updateRelaxedRigidityForceDirs(const std::vector<int> &f_idx);


  // engineered stiffness control:
  bool increaseStiffness(const std::vector<int> &j_idx,const Eigen::VectorXd &j_des, Eigen::VectorXd &last_jcmd);

  /** 
   * Increments the angles of joints until joint velocity is less than threshold or contact on fingertips is detected.
   * 
   * @param f_idx fingers to use for grasping.
   * @param j_idx joints to use for grasping.
   * @param thresh joint velocity threshold for detecting contact.
   * @param delta joint position increment
   * @param tactile_sensing if enabled, stop movement of a finger if a contact on its fingertip is detected.
   * 
   * @return 
   */
  bool graspControl(const std::vector<int> &f_idx, const std::vector<int> &j_idx, Eigen::VectorXd &last_pos_cmd, bool tactile_sensing=false);

  /** 
   * Moves the fingertips using Jacobian and joint velocity control until contact is detected.
   * 
   * @param tactile_sensing should biotac's be used for contact detection, if false, joint velocity will be used for contact detection.
   * @param fingers to use for approach vector control
   * @param approach_vectors 3d direction vectors for fingers.
   * 
   * @return 
   */
  bool approachContact(const bool &tactile_sensing , const std::vector<int> &fingers, const std::vector<Eigen::Vector3d> &approach_vectors,Eigen::VectorXd &last_pos_cmd);


  /** 
   * Increases joint position command using Jacobian along approach vector until desired force magnitude is reached
   * 
   * @param fingers 
   * @param mag_force 
   * @param approach_vectors 3d direction vectors for fingers.
   * @return true if contact force is reached for all fingers
   */
  bool forceServoing(const std::vector<int> &fingers, const std::vector<double> &mag_force, const std::vector<Eigen::Vector3d> approach_vectors);
  int grasp_tsteps_;
  double link_contact_thresh_;
  double grasp_delta_;
  double stiffness_delta_;

 protected:
  double approach_vel_;
 
 private:
  // used for getting transformations between frames
  tf::TransformListener tf_listener;

  /**
  * Returns rotation matrix from object_frame to world_frame. 
  **/
  const Eigen::MatrixXd get_rotation_matrix(const std::string object_frame, const std::string world_frame);

 /**
  * Returns R * Sx * transpose(R) where R is rotation matrix from object_frame to world_frame.
  **/
  const Eigen::MatrixXd get_omega_matrix(const std::string object_frame, const char* world_frame, const Eigen::MatrixXd selection_matrix);

 /**
  * Returns R * S * transpose(R) where R is parameter rotation_matrix and S is parameter selection_matrix.
  **/
  const Eigen::MatrixXd get_omega_matrix(const Eigen::MatrixXd selection_matrix, const Eigen::MatrixXd rotation_matrix);

 /** 
  * Returns 3 dimensional vector with x,y,z readings of force for a given fingertip id.
  */
  const Eigen::Vector3d get_current_force_readings(const int fingertip_id);

  /**
   *  Supports only allegro hand for now.
   *  @param joint_id value between 0 and 15
   */
  int joint_id_to_finger_id(int joint_id);

  /**
   * Returns 0 for a frame that contains "index", .., 3 for .. "thumb").
   */
  const int get_finger_id(const std::string frame_id);
  /**
   * "index_biotac_tip" for 0, ..., "thumb_biotac_tip" for 3.
   */
  const std::string finger_id_to_biotac_tip_frame_name(const int finger_id);
 
};
}
}
