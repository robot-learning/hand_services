# Hand Services

Contains controllers with API access for using the Allegro hand


## Contributors

-   [Balakumar Sundaralingam](<https://balakumar-s.github.io/>)


## License

-   BSD license (refer LICENSE file)


## Citing

This code was developed as part of the following publication. Please cite the below publication, if you use our source code.

*Sundaralingam B, Hermans T. Relaxed-rigidity constraints: kinematic trajectory optimization and collision avoidance for in-grasp manipulation. Autonomous Robots. 2019 Feb 15;43(2):469-83.*

```
@article{sundaralingam2019relaxed,
  title={Relaxed-rigidity constraints: kinematic trajectory optimization and collision avoidance for in-grasp manipulation},
  author={Sundaralingam, Balakumar and Hermans, Tucker},
  journal={Autonomous Robots},
  volume={43},
  number={2},
  pages={469--483},
  year={2019},
  publisher={Springer}
}
```


## How do I use it?

Use the SendHandCommand to communicate with the hand. Different control modes are available for the hand. Populate the service message based on the control mode. A brief overview is given here.

The different control modes are listed in the table below:

| Index | Mode                                                                  | Inputs                         | Returns        |
|------ |---------------------------------------------------------------------- |------------------------------- |--------------- |
| -1    | Immediate joint position (TODO)                                       | des\_js                        |                |
| 0     | Joint Position                                                        | des\_js                        |                |
| 1     | Joint trajectory(Use trajectory smoothing service for smooth)         | des\_jtraj,biotac              |                |
| 2     | Close hand                                                            | joints, delta, biotac          |                |
| 3     | Move fingertip in approach vector direction until contact is detected | fingers,data(approach vectors) |                |
| 4     | Increase grasp stiffness(Hack)                                        | fingers,des\_js,joints,delta   | last\_des\_cmd |
| 5     | Enable External Controller                                            | control\_mode                  |                |
| 6     | Disable External Controller                                           |                                |                |
| 7     | Biotac tare reset                                                     | fingers                        |                |
| 8     | Jacobian servoing for reaching a desired force                        |                                |                |


### Joint Position:

-   Set control\_mode=0
-   Set des\_js to have the desired joint positions for the hand.
-   Once the service is called, the service will return once the hand has reached the des\_js or


### Joint Trajectory:

The joint trajectory must be smooth for good performance. Use [trajectory\_smoothing](<https://github.com/balakumar-s/trajectory_smoothing>) to get smooth velocities for a joint position trajectory(usually obtained from a kinematic motion planner).

-   Set des\_jtraj to be the desired joint trajectory
-   Set biotac to True if you want the trajectory to stop on contact detection.

### Close Hand:

Closes all the fingers, can be used for grasping an object.

- Set biotac to True if you want that closing of a finger stops if its fingertip is in contact, or False to ignore tactile information


### Increase grasp stiffness:

This increments the joints by delta.


### Fingertip contact controller:

Move fingertip in approach vector direction using Jacobian psuedoinverse until contact is made.


### Feedback Controller:
