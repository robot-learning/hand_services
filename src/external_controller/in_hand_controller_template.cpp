#include <hand_services/external_controller/in_hand_controller_template.h>

namespace hand_task_controller
{

// controller functions

void inHandController::updateHook()
{

  robot_interface::ControlMode cm=robot_interface::ControlMode::POS;// 0==position

  Eigen::VectorXd q_cmd,qd_cmd;
  q_cmd.resize(16);
  
  qd_cmd.resize(16);
  while(nh_.ok())
  {
    qd_cmd.setZero();
    q_cmd.setZero();
  
    // main service call loop
    ros::spinOnce();
    if(controller_enabled_)
    {
      //
      if(get_control_cmd(q_cmd,qd_cmd))
      {
        // publish to robot:
        robot_interface_->publishRealRobot(q_cmd,cm);
      }
      // we maintain a control rate only when external_controller is enabled.
      //rate_.sleep();
    }
    // TODO: maybe we should maintain control rate all the time?

    rate_.sleep();

  }
}
bool inHandController::configureHook()
{
  if(!Controller::configureHook())
    return false;

  // read toggle parameter
  if(!nh_.getParam("toggle_srv_name",toggle_srv_name_))
  {
    return false;
  }

  control_toggle_req_=nh_.advertiseService(toggle_srv_name_,&inHandController::toggle_control,this);

  controller_enabled_=false;
  return true;
}
bool inHandController::toggle_control(hand_services::ToggleController::Request &req,hand_services::ToggleController::Response &res)
{
  if(req.enable_controller==true)
  {
    controller_enabled_=true;
    res.status=true;
    return true;
  }
  else 
  {
    controller_enabled_=false;
    res.status=true;
    return true;
  }
  res.status=false;
  return false;
}

}
