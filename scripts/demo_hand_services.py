#!/usr/bin/env python

# Copyright (C) 2018  Balakumar Sundaralingam, LL4MA Lab, University of Utah

# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import rospy
import numpy as np
import copy

from hand_client import *
from in_hand_task_control_client import *


if __name__=='__main__':
    rospy.init_node('hand_services_demo')

    fingers=[0,1,3]
    home_js=np.array([0.1,0.0,0.0,0.0,
                     0.1,0.0,0.0,0.0,
                     -0.2,0.0,0.0,0.0,
                      1.2,0.0,0.0,0.0])

    rmp_js=np.array([0.2,0.5,0.6,0.0,
                     0.2,0.5,0.6,0.9,
                     0.2,0.5,0.6,0.0,
                     1.3,0.0,0.3,0.0])
    des_js=np.array([0.2,0.0,0.0,0.0,
                     0.2,0.0,0.0,0.0,
                     0.0,0.0,0.0,0.0,
                     1.6,0.0,0.0,0.0])

    # send joint position command:
    
    hand_client=handClient()
    task_client=inHandClient()
    
    reset=hand_client.reset_tare([0,1,2,3])
    raw_input("Test JPos cmd?")
    reached=hand_client.send_pos_cmd(des_js)

    raw_input("Test force servoing cmd?")
    # thumb is fourth finger
    # args: finger chain indices, approach vector per finger, desired magnitude
    # thumb vector to go towards other fingers in palm_link frame [-0.7896412343978096, 0.07413330593725141, 0.6090738657088459]
    hand_client.force_servo([0,1,2,3], [0, 0, 0, 0, 0, 0, 0, 0, 0, -0.7896, 0.0741, 0.6091], [0, 0, 0, 1])
    
    raw_input('grasp object?')
    # grasp object:
    j_idx=[1,2,3, 5,6,7, 9,10,11, 13,14,15]
    hand_client.grasp_object(j_idx)

    q_des=hand_client.get_joint_state()
    
    #print q_des
    '''
    raw_input("increase stiffness")

    _,q_des=hand_client.increase_stiffness(j_idx,q_des)

    _,q_des=hand_client.increase_stiffness(j_idx,q_des)

    raw_input("increase stiffness")

    _,q_des=hand_client.increase_stiffness(j_idx,q_des)

    _,q_des=hand_client.increase_stiffness(j_idx,q_des)
    '''
    raw_input("Test JPos cmd?")
    reached=hand_client.send_pos_cmd(home_js)
    

    '''
    # test joint trajectory:
    ## create a desired hand position:
    

    # create trajectory:
    des_jtraj=JointTrajectory()

    jpt=JointTrajectoryPoint()
    jpt.positions=home_js
    des_jtraj.points.append(copy.deepcopy(jpt))

    jpt=JointTrajectoryPoint()
    jpt.positions=des_js
    des_jtraj.points.append(copy.deepcopy(jpt))


    # call trajectory smoothing service:
    sm_traj=hand_client.get_smooth_traj(des_jtraj)

    raw_input("send traj?")
    
    reached=hand_client.send_jtraj(sm_traj)
    '''
    '''
    #print reached
    # send joint trajectory command:
    init_js=home_js
    des_js=np.array([0.1,0.4,0.8,0.8,
                     0.1,0.4,0.8,0.8,
                     -0.2,0.4,0.8,0.8,
                     1.4,0.2,0.5,0.8])

    des_jtraj=JointTrajectory()

    n_steps=200

    for i in range(n_steps):
        jpt=JointTrajectoryPoint()
        jpt.positions=init_js+(des_js-init_js)*float(i+1)/n_steps
        des_jtraj.points.append(copy.deepcopy(jpt))

    #raw_input("Test Jtraj cmd?")
    #reached=hand_client.send_jtraj(des_jtraj)
    # reset tactile pressure tare:

    
    # send joint trajectory command with contact detection:
    #raw_input("Test Jtraj contact cmd?")s
    #reached=hand_client.send_jtraj(des_jtraj,True)
    raw_input("reset tactile tare?")
    reset=hand_client.reset_tare([0,1,2,3])

    # enable stiffness control:
    raw_input("enable stiffness control?")
    #stiff=hand_client.enable_stiffness([0,1,3])
    #print home_js
    stiff,last_cmd=hand_client.increase_stiffness([0,1,3],home_js)
    raw_input("enable stiffness control?")
    
    stiff,last_cmd=hand_client.increase_stiffness([0,1,3],last_cmd)

    # diable stiffness control:
    raw_input("disable stiffness control?")
    stiff=hand_client.disable_stiffness()
    '''
