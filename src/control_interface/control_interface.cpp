/*
 * Copyright (C) 2018  Balakumar Sundaralingam, LL4MA Lab, University of Utah
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hand_services/control_interface/control_interface.h>
#include <cmath>

using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::string;

namespace hand_services
{
namespace control_interface
{
bool controlInterface::configureHook()
{
  log("Initializing...");
  // setting all state flags to false:
  tactile_state_received_=false;
  
  
  if(!Controller::configureHook())
    return false;

  
  bool success = true;
  success &= nh_.getParam("tactile_enabled", tactile_enabled_);
  if (success) {log("True");} else {log("False");}
  success &= nh_.getParam("simulation", simulation_);
  if (success) {log("True");} else {log("False");}
  stiffness_control_=false;
  external_control_=false;

  if(tactile_enabled_)
  {
    log("Tactile enabled");
    
    if (simulation_)
    {
      log("This is running in simulation");
      success &= nh_.getParam("tactile_index_topic", tactile_index_topic_);
      success &= nh_.getParam("tactile_middle_topic", tactile_middle_topic_);
      success &= nh_.getParam("tactile_ring_topic", tactile_ring_topic_);
      success &= nh_.getParam("tactile_thumb_topic", tactile_thumb_topic_);
      tactile_sub_i_ = nh_.subscribe<geometry_msgs::WrenchStamped>(tactile_index_topic_, 1, &controlInterface::indexTactileCallbackSimulation, this);
      tactile_sub_m_ = nh_.subscribe<geometry_msgs::WrenchStamped>(tactile_middle_topic_, 1, &controlInterface::middleTactileCallbackSimulation, this);
      tactile_sub_r_ = nh_.subscribe<geometry_msgs::WrenchStamped>(tactile_ring_topic_, 1, &controlInterface::ringTactileCallbackSimulation, this);
      tactile_sub_t_ = nh_.subscribe<geometry_msgs::WrenchStamped>(tactile_thumb_topic_, 1, &controlInterface::thumbTactileCallbackSimulation, this);
    } 
    else
    {
      log("This is running on the real robot");
      success &= nh_.getParam("tactile_sub_topic", tactile_sub_topic_);
      tactile_sub_=nh_.subscribe<biotac_sensors::BioTacForce>(tactile_sub_topic_, 1, &controlInterface::tactileCallback, this);
    }

    kp_.resize(4);
    kd_.resize(4);
    success &= getGainsFromParamServer("stiffness/p_gains",nh_,kp_);
    success &= getGainsFromParamServer("stiffness/d_gains",nh_,kd_);
    success &= nh_.getParam("tactile_contact_probability",contact_prob_);
    success &= nh_.getParam("tactile_tare_srv",tare_reset_name_);
    update_virtual_frame_=true;
    tare_reset_srv_=nh_.serviceClient<biotac_sensors::UpdateTare>(tare_reset_name_);
  } else { log("Tactile disabled.");}

  success &= nh_.getParam("hand_command_srv",hand_cmd_srv_);
  success &= nh_.getParam("goal_threshold",goal_thresh_);
  success &= nh_.getParam("cmd_timeout",cmd_timeout_);

  // Load grasp parameters:
  success &= nh_.getParam("grasp_timesteps",grasp_tsteps_); 
  success &= nh_.getParam("grasp_delta_v",grasp_delta_);
  if(std::abs(grasp_delta_)>0.1)
  {
    grasp_delta_=0.1;
    cerr<<"Error grasp_delta_ is greater than 0.1 rad/s"<<endl;
    assert(false);
  }
  success &= nh_.getParam("link_contact_thresh",link_contact_thresh_);
  success &= nh_.getParam("stiffness_delta",stiffness_delta_);
  success &= nh_.getParam("approach_vel",approach_vel_);
  
  if(!success)
  {
    log("Failed to load params from param server.", ERROR);
    return false;
  }

  // initialize service request:
  control_req_=nh_.advertiseService(hand_cmd_srv_,&controlInterface::handCommand,this);
  
  log("Initialization complete.");
  return true;  
}

bool controlInterface::startHook()
{
  return true;
}
void controlInterface::updateHook()
{
  robot_interface::ControlMode cm=robot_interface::ControlMode::POS;// 0==position
  
  while(nh_.ok())
  {
    // main service call loop
    ros::spinOnce();
    if(external_control_)
    {
      //cerr<<ext_j_cmd.size()<<endl;
      if(ext_j_cmd.size()==16 && ext_jdot_cmd.size()==16)
      {
        //log("publishing");
        // publish to robot:
        //cerr<<ext_j_cmd.transpose()<<endl;
        robot_interface_->publishRealRobot(ext_j_cmd,ext_jdot_cmd);
      }
      else if(ext_j_cmd.size()==16 )
      {
        robot_interface_->publishRealRobot(ext_j_cmd,cm);
      
      }
        
      // we maintain a control rate only when external_controller is enabled.
      rate_.sleep();
    }

  }
}
void controlInterface::stopHook()
{
}
void controlInterface::cleanupHook()
{
}

void controlInterface::indexTactileCallbackSimulation(const geometry_msgs::WrenchStamped::ConstPtr& msg)
{
 tactileCallbackSimulation(msg);
}

void controlInterface::middleTactileCallbackSimulation(const geometry_msgs::WrenchStamped::ConstPtr& msg)
{
 tactileCallbackSimulation(msg);
}
void controlInterface::ringTactileCallbackSimulation(const geometry_msgs::WrenchStamped::ConstPtr& msg)
{
 tactileCallbackSimulation(msg);
}
void controlInterface::thumbTactileCallbackSimulation(const geometry_msgs::WrenchStamped::ConstPtr& msg)
{
 tactileCallbackSimulation(msg);
}

void controlInterface::tactileCallbackSimulation(const geometry_msgs::WrenchStamped::ConstPtr& msg)
{
  tactile_data_.resize(4);
  // 4 fingers: x,y,z
  // array is filled as [f1_x, f1_y, f1_z, f2_x, f2_y, f2_z, f3_x, f3_y, f3_z, f4_x, f4_y, f4_z]
  tactile_force_data_.resize(4 * 3);
  int finger_id = get_finger_id(msg->header.frame_id);

  // read forces
  tactile_force_data_[finger_id*3 + 0] = msg->wrench.force.x;
  tactile_force_data_[finger_id*3 + 1] = msg->wrench.force.y;
  tactile_force_data_[finger_id*3 + 2] = msg->wrench.force.z;

  tactile_state_received_=true;

  // are they in contact?
  const float tresh = 0.01;
  if (msg->wrench.force.x > tresh ||
      msg->wrench.force.y > tresh || 
      msg->wrench.force.z > tresh){
    tactile_data_[finger_id]=1;
  }
}


void controlInterface::tactileCallback(const biotac_sensors::BioTacForce::ConstPtr& msg)
{
  tactile_data_.resize(msg->contact.data.size());
  tactile_force_data_.resize(msg->forces.size() * 3);

  for(int i=0;i<msg->contact.data.size();++i)
  {
    tactile_data_[i]=msg->contact.data[i];
    float x = msg->forces[i].wrench.force.x; 
    float y = msg->forces[i].wrench.force.y;
    float z = msg->forces[i].wrench.force.z;
    tactile_force_data_[i*3 + 0] = x;
    tactile_force_data_[i*3 + 1] = y;
    tactile_force_data_[i*3 + 2] = z;
  }
  tactile_state_received_=true;
}

void controlInterface::externalCmdCallback(sensor_msgs::JointState msg)
{
  //cerr<<msg->position.size()<<endl;
  // get new state:
  if(!msg.position.empty())
  {
    ext_j_cmd.resize(msg.position.size());
    for(int i=0;i<msg.position.size();++i)
    {
      ext_j_cmd[i]=msg.position[i];
    }
  }
  if(!msg.velocity.empty())
  {
    ext_jdot_cmd.resize(msg.velocity.size());
    for(int i=0;i<msg.velocity.size();++i)
    {
      ext_jdot_cmd[i]=msg.velocity[i];
    }

  }
  
  
}
bool controlInterface::handCommand(hand_services::SendHandCommand::Request &req, hand_services::SendHandCommand::Response &resp)
{

  // hand command processing function:
  vector<int> fingers(req.fingers.size(),0);
  for(int i=0;i<fingers.size();++i)
    fingers[i]=req.fingers[i];

  
  if(req.control_mode==0)
  {
    //cerr<<num_jnts_<<endl;
    log("Runnning Joint position control mode");
    if(req.des_js.position.size()==num_jnts_)
    {
    Eigen::Map<Eigen::VectorXd> q_des(&req.des_js.position[0],req.des_js.position.size());
    resp.reached=sendDesJPos(q_des);
    }
    else
    {
      log("Desired joint position command has incorrect number of joints!!",ERROR);
    }
    resp.last_des_cmd=req.des_js;
  }
  if(req.control_mode==1)
  {
         
    log("Runnning Joint position trajectory control mode");
    if(tactile_enabled_ && req.biotac)
    {
      log("tactile sensing active");
      
      resp.reached=sendTrajectory(req.des_jtraj,true,fingers);
    }
    else
    {
      if(req.biotac)
        log("Tactile sensor not initialized, running trajectory without contact sensing",WARN);
      
      resp.reached=sendTrajectory(req.des_jtraj,false,fingers);
    }
  }
  if(req.control_mode==2)
  {
    bool tactile_sensing = false;
    if(tactile_enabled_ && req.biotac)
    {
      log("Grasping object with tactile sensing");
      tactile_sensing = true;
    }
    else
    {
      log("Grasping object without tactile sensing");
    }
    vector<int> f_idx{ 0,1,2,3 };
    vector<int> j_idx(req.joints.size(),0);
    for(int i=0;i<req.joints.size();++i)
      j_idx[i]=req.joints[i];
    Eigen::VectorXd q_current_cmd;
    graspControl(f_idx,j_idx,q_current_cmd,tactile_sensing);
    sensor_msgs::JointState last_cmd;
    last_cmd.position.resize(q_current_cmd.size());
    for(int i=0;i<q_current_cmd.size();++i)
    {
      last_cmd.position[i]=q_current_cmd[i];
    }
    resp.last_des_cmd=last_cmd;
  }
  if(req.control_mode==3)
  {
    log("Jacobian Contact Controller");
      // Move to initial joint angles:
      //Map<Eigen::VectorXd> q_des(&req.des_js.position[0],req.des_js.position.size());
      //resp.reached=sendDesJPos(q_des);
      //log("moved to initial joint config");
      // get approach vector from serialized data:
      vector<Eigen::Vector3d> app_vectors(req.fingers.size());
      for(int i=0;i<req.fingers.size();++i)
      {
        app_vectors[i][0]=req.data[i*3];
        app_vectors[i][1]=req.data[i*3+1];
        app_vectors[i][2]=req.data[i*3+2];
      }
      Eigen::VectorXd q_current_cmd;

      // Run Jacobian controller until contact is detected:
      bool contact=approachContact(req.biotac,fingers,app_vectors,q_current_cmd);
      sensor_msgs::JointState last_cmd;
      last_cmd.position.resize(q_current_cmd.size());
      for(int i=0;i<q_current_cmd.size();++i)
      {
        last_cmd.position[i]=q_current_cmd[i];
      }
      resp.last_des_cmd=last_cmd;

    // Run force threshold controller
    
  }

  if(req.control_mode==4)
  {
    log("increasing stiffness");
    Eigen::Map<Eigen::VectorXd> q_des(&req.des_js.position[0],req.des_js.position.size());
    Eigen::VectorXd j_cmd=q_des;
    vector<int> j_idx(req.joints.size(),0);
    for(int i=0;i<req.joints.size();++i)
      j_idx[i]=req.joints[i];

    resp.reached=increaseStiffness(j_idx,q_des,j_cmd);
    sensor_msgs::JointState last_cmd;
    last_cmd.position.resize(j_cmd.size());
    for(int i=0;i<j_cmd.size();++i)
    {
      last_cmd.position[i]=j_cmd[i];
    }
    resp.last_des_cmd=last_cmd;
  }

  if(req.control_mode==5)
  {
    log("Connecting to external controller");
    // subscribe to external controller command topic:
    if(!req.string_data.empty())
    {

      external_cmd_sub_=nh_.subscribe<sensor_msgs::JointState>(req.string_data[0], 1, &controlInterface::externalCmdCallback, this);
      external_control_=true;
    }
    else
    {
      log("External joint cmd topic not specified!!!");
    }

  }
  if(req.control_mode==6)
  {
    log("Disconnecting from external controller");
    external_cmd_sub_.shutdown();
    external_control_=false;
  }

  if(req.control_mode==7)
  {
    log("resetting biotac tare");
    biotac_sensors::UpdateTare tare_msg;
    tare_msg.request.fingers=req.fingers;
    if(tare_reset_srv_.call(tare_msg))
    {
      resp.reached=tare_msg.response.updated;
    }
    else
    {
      log("Failed to call tare reset service",ERROR);
    }
  }

  if(req.control_mode==8)
  {
    log("Reaching desired force on fingertips by Jacobian servoing");
    vector<Eigen::Vector3d> app_vectors(req.fingers.size());
    vector<double> force_mag(req.fingers.size());

    for(int i=0;i<req.fingers.size();++i)
    {
      app_vectors[i][0]=req.data[i*3];
      app_vectors[i][1]=req.data[i*3+1];
      app_vectors[i][2]=req.data[i*3+2];
      force_mag[i]=req.data[req.fingers.size()*3+i];
    }

    bool force_reached=forceServoing(fingers,force_mag,app_vectors);
  }
  
  // fingers desired position with sliding 
  if(req.control_mode==9)
  {
    log("Runnning 'Joint position while sliding' control mode");
    if(!tactile_enabled_) {
        log("Cannot slide because tactile is not enabled", ERROR);
        return false;
    }
  

    /* 00000000000000000000000000000000000000000000
         ---------- computing error ------------ 
       00000000000000000000000000000000000000000000*/

    // 4 fingers: x,y,z in palm link frame 
    Eigen::VectorXd x_desired(4*3);
    Eigen::VectorXd x_current(4*3);

    Eigen::VectorXd x_tilde(4*3); // = x_desired - x_current 
    Eigen::VectorXd f_tilde(4*3); // = f_desired - f_current 
    
    Eigen::VectorXd q_dot_cmd(16);
    Eigen::VectorXd q_cmd(16);

    // compute FK for all fingers 
    Eigen::VectorXd fk_pose;
    for(int i=0; i<robot_interface_->kdl_->getNumChains(); i++) {
        robot_interface_->kdl_->getFK(i, q_, fk_pose); // position (x,y,z) + orientation (x,y,z,w) in 'palm_link' frame
        x_current[i*3] = fk_pose[0];
        x_current[i*3+1] = fk_pose[1];
        x_current[i*3+2] = fk_pose[2];
    }


    for(int i = 0; i < 4; i++) {
        // compute x_tilde = desired - current position in 'palm_link' frame
        x_desired[i*3+0] = req.desired_positions[i*3+0];
        x_desired[i*3+1] = req.desired_positions[i*3+1];
        x_desired[i*3+2] = req.desired_positions[i*3+2];

        x_tilde[i*3 + 0] = x_desired[i*3 + 0] - x_current[i*3 + 0];
        x_tilde[i*3 + 1] = x_desired[i*3 + 1] - x_current[i*3 + 1];
        x_tilde[i*3 + 2] = x_desired[i*3 + 2] - x_current[i*3 + 2];

        // read current force in fingertip frame
        Eigen::Vector3d f_current = get_current_force_readings(i);

        Eigen::Vector3d desired_forces;
        desired_forces[0] = req.desired_forces[i*3 + 0];
        desired_forces[1] = req.desired_forces[i*3 + 1];
        desired_forces[2] = req.desired_forces[i*3 + 2];

        Eigen::Matrix3d rotation_matrix = get_rotation_matrix(
                                            finger_id_to_biotac_tip_frame_name(i),
                                            "object_frame_finger" + std::to_string(i));

        // rotate read forces from fingertip frame to object frame
        Eigen::Vector3d f_desired = rotation_matrix * desired_forces; 

        // compute force error in object frame
        f_tilde[i*3 + 0] = f_desired[0] - f_current[0];
        f_tilde[i*3 + 1] = f_desired[1] - f_current[1];
        f_tilde[i*3 + 2] = f_desired[2] - f_current[2];
    }

    Eigen::VectorXd left_term_error(3);
    Eigen::VectorXd right_term_error(3);
    Eigen::VectorXd total_error(12);

    Eigen::MatrixXd S_x(3,3); 
    S_x << 1,0,0,
            0,1,0,
            0,0,0;
    Eigen::MatrixXd S_f = Eigen::MatrixXd::Identity(3,3) - S_x;

    Eigen::VectorXd x_tilde_one_finger(3); 
    Eigen::VectorXd f_tilde_one_finger(3); 

    // compute omega(Sx) * x_tilde + omega(Sf) * R * f_tilde for every finger
    for(int i = 0; i < 4; i++) {
       // left term
       x_tilde_one_finger[0] = x_tilde[i*3 + 0];
       x_tilde_one_finger[1] = x_tilde[i*3 + 1];
       x_tilde_one_finger[2] = x_tilde[i*3 + 2];
 
       Eigen::MatrixXd omega_x = get_omega_matrix("object_frame_finger" + std::to_string(i), "palm_link", S_x);
       left_term_error = omega_x * x_tilde_one_finger; 

       // right term
       f_tilde_one_finger[0] = f_tilde[i*3 + 0];
       f_tilde_one_finger[1] = f_tilde[i*3 + 1];
       f_tilde_one_finger[2] = f_tilde[i*3 + 2];
 
       Eigen::MatrixXd rotation_matrix = get_rotation_matrix("object_frame_finger" + std::to_string(i), "palm_link");
       Eigen::MatrixXd omega_f = get_omega_matrix(S_f, rotation_matrix);
       right_term_error = omega_f * rotation_matrix * f_tilde_one_finger; 

       // debugging purposes
       for(int j = 0; j < 3; j++) {
           cout << "left term, right term" << endl;
           cout << left_term_error[j] << "," << right_term_error[j] << endl;
       }

       // complete term TODO: scale/weight?
       total_error[i*3 + 0] = left_term_error[0] + 10*right_term_error[0];
       total_error[i*3 + 1] = left_term_error[1] + 10*right_term_error[1];
       total_error[i*3 + 2] = left_term_error[2] + 10*right_term_error[2];
    }

   

    /* 000000000000000000000000000000000000000000000000000000
       ----- converting error to desired joint positions ---- 
       000000000000000000000000000000000000000000000000000000 */

    Eigen::VectorXd x_dot_desired = total_error;
    Eigen::MatrixXd J_mat;
    Eigen::MatrixXd J_mat_psinverse;
    Eigen::VectorXd result(4);
    Eigen::MatrixXd q_dot_finger_null;

    Eigen::MatrixXd home_mat(4,4); 
    home_mat << 0.1,0.0,0.0,0.0, 
                0.1,0.0,0.0,0.0, 
               -0.2,0.0,0.0,0.0, 
                1.2,0.0,0.0,0.0; 
    Eigen::MatrixXd I_N_(4,4); 
    I_N_ << 1,0,0,0,
            0,1,0,0,
            0,0,1,0,
            0,0,0,1;

    for(int i = 0; i < 4; i++) {
        //cout << "Getting Jacobian for Finger " << std::to_string(i) << endl;
        robot_interface_->kdl_->getJacobian(i,q_.segment(i*4,4),J_mat);

        robot_interface_->getPseudoInverse(J_mat, J_mat_psinverse, 1.e-5);

        Eigen::VectorXd x_dot_desired_one_finger(6);
        //xyz desired, rotations error to 0
        x_dot_desired_one_finger[0] = x_dot_desired[i*3 + 0];
        x_dot_desired_one_finger[1] = x_dot_desired[i*3 + 1];
        x_dot_desired_one_finger[2] = x_dot_desired[i*3 + 2];
        x_dot_desired_one_finger[3] = 0; //TODO: current rotation instead? (probably)
        x_dot_desired_one_finger[4] = 0;
        x_dot_desired_one_finger[5] = 0;
        x_dot_desired_one_finger = x_dot_desired_one_finger.transpose();

        result = J_mat_psinverse * x_dot_desired_one_finger;
     
        //equation (25) in "Control of Reundant Robot Manipulators" by Tucker Hermans, 2019.
        //null space resolution
        Eigen::VectorXd home_finger = home_mat.row(i);
        Eigen::VectorXd current_finger = q_.segment(i*4,4);
        q_dot_finger_null = (I_N_ - J_mat_psinverse * J_mat) * (home_finger - current_finger);

        for (int j = 0; j < 4; j++) {
            q_dot_cmd[i*4 + j] = result[j] + q_dot_finger_null(j,0);
        }
     }
 
     robot_interface::ControlMode cm=robot_interface::ControlMode::POS;// 0==position

     // desired position = current position + desired velocity    
     q_cmd = q_ + q_dot_cmd;

     robot_interface_->clipJointsValues(q_cmd);

     //TODO: send q_dot_cmd and q_cmd
     //TODO: Control Mode POS_AND_VEL
     resp.reached = sendDesJPos(q_cmd);
  }

  if(req.control_mode==-1)
  {
    log("Immediate joint position command is currently disabled");
    resp.reached=false;
  }
  return true;
}

bool controlInterface::sendDesJPos(const Eigen::VectorXd &des_js)
{

  // TODO: Check if desired joint position is within robot joint limits
  
  robot_interface::ControlMode cm=robot_interface::ControlMode::POS;// 0==position

  // Slowly move to desired joint state from current joint state:
  Eigen::VectorXd cmd;
  int i=0;
  while(nh_.ok() && i<200)
  {
    
    cmd=q_+(des_js-q_)*double(i)/200.0;
    robot_interface_->publishRealRobot(cmd,cm);
    i++;
    rate_.sleep();
  }
  // command final joint configuration:
  ros::Time begin = ros::Time::now();
  ros::Time current = ros::Time::now();
 
  cmd=des_js;
  bool reached=false;
  double err;
  while(nh_.ok() && !reached && (current.toSec()-begin.toSec())<cmd_timeout_)
  {
    robot_interface_->publishRealRobot(cmd,cm);
    rate_.sleep();
    ros::spinOnce();
    reached=computeError(des_js,err);
    current = ros::Time::now();
  }
  if(!reached)
  {
    std::ostringstream sstream;
    sstream << "Goal could not be reached, Final error: "<<err;
    std::string msg=sstream.str();
    log(msg);
  }
  else
  {
    std::ostringstream sstream;
    sstream << "Goal reached, Final error: "<<err;
    std::string msg=sstream.str();
    log(msg);

  }
  return reached;
}

bool controlInterface::sendTrajectory(trajectory_msgs::JointTrajectory &jtraj, bool contact_sensing, const vector<int> &fingers)
{

  robot_interface::ControlMode cm=robot_interface::ControlMode::POS;// 0==position

  // Move robot current state to jtraj initial state:
  Eigen::Map<const Eigen::VectorXd> j_init(&jtraj.points[0].positions[0],jtraj.points[0].positions.size());
  if(jtraj.points[0].velocities.size()<j_init.size())
  {
    log("Joint Trajectory velocity array size is less than number of joints(Maybe empty)!",ERROR);
    return false;
  }
  Eigen::Map<const Eigen::VectorXd> jdot_init(&jtraj.points[0].velocities[0],jtraj.points[0].velocities.size());
  
  //log("moving to initial state");
  //sendDesJPos(j_init);
  
  contact_des_jcmd_=j_init;
  first_contact_.clear();
  first_contact_.resize(4,false);
  ros::spinOnce();
  Eigen::VectorXd j_cmd=j_init;
  Eigen::VectorXd jdot_cmd=jdot_init;
  jdot_cmd.setZero();
  for(int i=0;i<jtraj.points.size();++i)
  {
    Eigen::Map<const Eigen::VectorXd> j_cmd_des(&jtraj.points[i].positions[0],jtraj.points[i].positions.size());
    Eigen::Map<const Eigen::VectorXd> jdot_cmd_des(&jtraj.points[i].velocities[0],jtraj.points[i].velocities.size());
    
    
    // Only sending joint command to fingers requested
    if(!contact_sensing)
    {
      for(int j=0;j<fingers.size();++j)
      {
        j_cmd.segment(fingers[j]*4,4)=j_cmd_des.segment(fingers[j]*4,4);
        jdot_cmd.segment(fingers[j]*4,4)=jdot_cmd_des.segment(fingers[j]*4,4);
                
      }
    }
    
    if(contact_sensing)
    {
      j_cmd=contact_des_jcmd_;
      for(int j=0;j<fingers.size();++j)
      {
        if(!checkContact(fingers[j]) && !first_contact_[fingers[j]])
        {
          j_cmd.segment(fingers[j]*4,4)=j_cmd_des.segment(fingers[j]*4,4);
          jdot_cmd.segment(fingers[j]*4,4)=jdot_cmd_des.segment(fingers[j]*4,4);

        }
      }
          
    }
    robot_interface_->publishRealRobot(j_cmd,jdot_cmd);
    ros::spinOnce();
    rate_.sleep();
  
    if(contact_sensing)
    {
      for(int j=0;j<fingers.size();++j)
      {
        if(checkContact(fingers[j]) && !first_contact_[fingers[j]])
        {
          first_contact_[fingers[j]]=true;
          contact_des_jcmd_.segment(fingers[j]*4,4)=j_cmd.segment(fingers[j]*4,4);
        }
          
      }
    }
  }

  // Switching to position mode on the allegro hand for compliance:
  // TODO: Check if this is the right control structure
  robot_interface_->publishRealRobot(j_cmd,cm);
  
  double err;
  
  // check if all fingers are in contact/ reached desired goal state
  ros::Time begin = ros::Time::now();
  ros::Time current = ros::Time::now();
  
  bool reached_goal=false; 
  while(nh_.ok() &&  (current.toSec()-begin.toSec())<cmd_timeout_)
  {
    Eigen::Map<const Eigen::VectorXd> j_des(&jtraj.points[jtraj.points.size()-1].positions[0], jtraj.points[jtraj.points.size()-1].positions.size());
    //robot_interface_->publishRealRobot(j_des,cm);
    
    ros::spinOnce();
    rate_.sleep();

    reached_goal=computeError(j_des,err);
    
    current = ros::Time::now();

    if(contact_sensing)
    {
      
    }
  }
  if(!reached_goal && (current.toSec()-begin.toSec())>=cmd_timeout_)
  {
    std::ostringstream sstream;
    sstream << "Goal could not be reached, Final error: "<<err;
    std::string msg=sstream.str();
    log(msg);

  }
  else
  {
    std::ostringstream sstream;
    sstream << "Goal reached, Final error: "<<err;
    std::string msg=sstream.str();
    log(msg);
  }
  return true;
}
bool controlInterface::increaseStiffness(const vector<int> &j_idx,const Eigen::VectorXd &j_des, Eigen::VectorXd &q_des)
{
  ros::spinOnce();
  robot_interface::ControlMode cm=robot_interface::ControlMode::POS;// 0==position
  for(int i=0; i<j_idx.size();++i)
  {
    q_des[j_idx[i]]+=stiffness_delta_;
  }
  robot_interface_->publishRealRobot(q_des,cm);
  
  return true;
}

bool controlInterface::checkContact(int f_idx)
{
  assert(tactile_state_received_);
  if(tactile_data_[f_idx]<1)
  {
    return false;
  }
  return true;
}
bool controlInterface::computeError(const Eigen::VectorXd &des_js, double &err)
{
  // current joint state:
  err=(des_js-q_).squaredNorm();
  if(err>goal_thresh_)
    return false;
  else
    return true;
}

bool controlInterface::resetTactileTare(const vector<int> &f_idx)
{
  
}

void controlInterface::log(std::string msg)
{
  log(msg, INFO);
}


void controlInterface::log(std::string msg, LogLevel level)
{
  switch(level)
  {
    case WARN :
    {
      ROS_WARN_STREAM("[controlInterface] " << msg);
      break;
    }
    case ERROR :
    {
      ROS_ERROR_STREAM("[controlInterface] " << msg);
      break;    
    }
    default:
    {
      ROS_INFO_STREAM("[controlInterface] " << msg);
      break;    
    }
  }
}

int controlInterface::joint_id_to_finger_id(int joint_id)
{
  // allegro hand
  return joint_id/4;
}

bool controlInterface::graspControl(const vector<int> &f_idx, const vector<int> &j_idx,Eigen::VectorXd &last_pos_cmd, bool tactile_sensing)
{
  ros::spinOnce();
  robot_interface::ControlMode cm=robot_interface::ControlMode::POS;// 0==position
  vector<bool> link_contact(j_idx.size(),false);
  vector<bool> fingertip_contact(f_idx.size(), false);
  int j=0;
  int fingertip_id;
  bool all_links_contact=false;
  bool all_fingertips_contact=false;
  int contact_cnt=0;
  int fingertip_contact_counter=0;
  Eigen::VectorXd q_current_cmd=q_;
  Eigen::VectorXd q_dot_cmd(16);
  q_dot_cmd.setZero();

  if (tactile_sensing) { log("Tactile sensing enabled"); }
  while(nh_.ok() && j<grasp_tsteps_ && !all_links_contact && !all_fingertips_contact)
  {
    for(int i=0;i<j_idx.size();++i)
    {
      if(!link_contact[i])
      {
        if(!tactile_sensing)
        {
          q_dot_cmd[j_idx[i]]=grasp_delta_;
        }
        else if(tactile_sensing && !fingertip_contact[joint_id_to_finger_id(j_idx[i])])
        {
          q_dot_cmd[j_idx[i]]=grasp_delta_;
        }
      }
    }
    
    q_current_cmd+=q_dot_cmd;
    robot_interface_->publishRealRobot(q_current_cmd,cm);
    rate_.sleep();

    ros::spinOnce();
    // check which biotacs are active
    if (tactile_sensing) {
      for(int i=0;i<j_idx.size();++i)
      {
        fingertip_id = joint_id_to_finger_id(j_idx[i]);
        if(checkContact(fingertip_id)) 
        {
          q_dot_cmd[j_idx[i]]=0.0;
          if(!fingertip_contact[fingertip_id]){
            fingertip_contact[fingertip_id]=true;
            log("new fingertip in contact");
            fingertip_contact_counter++;
          }
        }
      }
    }

    // check if velocities are large enough after some time (300 iterations)
    if(j>300)
    {
      for(int i=0;i<j_idx.size();++i)
      {
        if(std::abs(q_dot_[j_idx[i]])<link_contact_thresh_ && !link_contact[i])
        {
          link_contact[i]=true;
          log("new link in contact");
          q_dot_cmd[j_idx[i]]=0.0;
          contact_cnt++;
        }
      }
    }

    j++;
    if(contact_cnt==j_idx.size())
    {
      all_links_contact=true;
      log("All links are in contact, stopping the grasp");
      break;
    }
    if(tactile_sensing && fingertip_contact_counter==f_idx.size())
    {
      all_fingertips_contact=true;
      log("All fingertips are in contact, stopping the grasp");
      break;
    }
  }
  // once all links/fingertips are in contact, switch to position command:
  ros::spinOnce();
  cm=robot_interface::ControlMode::POS;
  robot_interface_->publishRealRobot(q_current_cmd,cm);
  last_pos_cmd=q_current_cmd;

  return true;
}


bool controlInterface::approachContact(const  bool &tactile_sensing, const vector<int> &fingers, const vector<Eigen::Vector3d> &approach_vectors,Eigen::VectorXd &last_pos_cmd)
{
  // switch to velocity control:
  Eigen::VectorXd j_cur;
  Eigen::VectorXd q_dot_des(4);
  q_dot_des.setZero();
  Eigen::VectorXd q_dot_cmd(16);
  q_dot_cmd.setZero();
  Eigen::MatrixXd J_mat;

  
  robot_interface::ControlMode cm=robot_interface::ControlMode::POS;// 0==position

  bool contact_reached=false;
  Eigen::VectorXd fingers_contact(fingers.size());
  fingers_contact.setZero();
  Eigen::VectorXd q_current_cmd=q_;

  while(fingers_contact.sum()<fingers.size())
  {
    ros::spinOnce();

    for(int i=0;i<fingers.size();++i)
    {
      if(fingers_contact[i]==0)
      {
    
        // check for contact
        contact_reached=checkContact(fingers[i]);
        if(contact_reached)
        {
          fingers_contact[i]=1;
          q_dot_cmd.segment(4*fingers[i],4).setZero();
        }
        else
        {
          robot_interface_->kdl_->getJacobian(fingers[i],q_.segment(fingers[i]*4,4),J_mat);
      
          // TODO: J sandwich controller?
          //cerr<<J_mat.block(0,0,3,J_mat.cols())<<endl;
          //cerr<<approach_vectors[i]<<endl;
          q_dot_des=J_mat.block(0,0,3,J_mat.cols()).transpose()*approach_vectors[i];
          q_dot_des=approach_vel_*q_dot_des/q_dot_des.lpNorm<Eigen::Infinity>();

          //cerr<<q_dot_des.transpose()<<endl;
          // TODO: read number of finger joints?
          q_dot_cmd.segment(4*fingers[i],4)=q_dot_des.transpose();
        }
      }
    }
    // send to robot:
    q_current_cmd+=q_dot_cmd;

    robot_interface_->publishRealRobot(q_current_cmd,cm);

    rate_.sleep();
    
  }
  Eigen::VectorXd q_des=q_current_cmd;
  cm=robot_interface::ControlMode::POS;
  robot_interface_->publishRealRobot(q_des,cm);

  last_pos_cmd=q_current_cmd;

  
}
bool controlInterface::forceServoing(const vector<int> &fingers, const vector<double> &mag_force, const vector<Eigen::Vector3d> approach_vectors)
{
  robot_interface::ControlMode cm=robot_interface::ControlMode::POS;// 0==position
  //Eigen::VectorXd q_init=q_;
  Eigen::VectorXd q_cmd=q_;
  
  //Eigen::VectorXd q_des_f(4);
  //q_des_f.setZero();
  Eigen::VectorXd q_delta(4);
  q_delta.setZero();
  Eigen::MatrixXd J_mat;
  int nr_fingers = fingers.size();

  bool fingers_reached_force[nr_fingers] = {false};
  bool force_reached = false;
  bool all_fingers_reached_force = false;

  while(! all_fingers_reached_force)
  {
    ros::spinOnce();
    for(int i=0;i<nr_fingers;++i)
    {
      //cout<<"Current tacitle force data for finger " << std::to_string(i) << " is " << std::to_string(tactile_force_data_[fingers[i]]) << endl;
      double force_reached = tactile_force_data_[fingers[i]*3 + 0] * tactile_force_data_[fingers[i]*3 + 0] +
                             tactile_force_data_[fingers[i]*3 + 1] * tactile_force_data_[fingers[i]*3 + 1] + 
                             tactile_force_data_[fingers[i]*3 + 2] * tactile_force_data_[fingers[i]*3 + 2];  
      force_reached = tactile_force_data_[fingers[i]]>=mag_force[i];

      if(force_reached)
      {
        //cout<<"Finger " << std::to_string(fingers[i]) << " reached desired force. Current force: " << std::to_string(tactile_force_data_[fingers[i]]) << endl;
        fingers_reached_force[i] = true;
      } else {
        robot_interface_->kdl_->getJacobian(fingers[i],q_.segment(fingers[i]*4,4),J_mat);
        q_delta=J_mat.block(0,0,3,J_mat.cols()).transpose()*approach_vectors[i];
        q_cmd.segment(fingers[i]*4,4)+=q_delta.transpose();
        fingers_reached_force[i] = false;
      }

      // check if all fingers reached desired force
      for(int i=0; i<nr_fingers; i++)
      {
        all_fingers_reached_force = true;
        if(!fingers_reached_force[i])
        {
          all_fingers_reached_force = false;
          break;
        }
      }
      if(all_fingers_reached_force) {break;}
    }
    robot_interface_->publishRealRobot(q_cmd,cm);
    rate_.sleep();
  }
  cout << "All fingers reached desired forces" << endl;
}

const Eigen::MatrixXd controlInterface::get_rotation_matrix(const string object_frame, const string world_frame) {
        tf::StampedTransform transform_obj_to_w; 
        try {
            tf_listener.lookupTransform(object_frame, world_frame, ros::Time(0), transform_obj_to_w);
        }
        catch (tf::TransformException ex) {
            log(ex.what(), ERROR);
        }

        tf::Matrix3x3 rotation_obj_to_w(transform_obj_to_w.getRotation());
        Eigen::Matrix3d rotation_o_to_w;
        tf::matrixTFToEigen(rotation_obj_to_w, rotation_o_to_w);
        return rotation_o_to_w;
}

const Eigen::MatrixXd controlInterface::get_omega_matrix(const string object_frame, const char* world_frame, const Eigen::MatrixXd selection_matrix){
        Eigen::Matrix3d rotation_o_to_w = get_rotation_matrix(object_frame, world_frame);            
        return rotation_o_to_w * selection_matrix * rotation_o_to_w.transpose();
}
 
const Eigen::MatrixXd controlInterface::get_omega_matrix(const Eigen::MatrixXd selection_matrix, const Eigen::MatrixXd rotation_o_to_w){
        return rotation_o_to_w * selection_matrix * rotation_o_to_w.transpose();
}

const Eigen::Vector3d controlInterface::get_current_force_readings(const int fingertip_id){
    Eigen::Vector3d read_forces;
    read_forces[0] = tactile_force_data_[fingertip_id*3 + 0]; 
    read_forces[1] = tactile_force_data_[fingertip_id*3 + 1]; 
    read_forces[2] = tactile_force_data_[fingertip_id*3 + 2]; 
    return read_forces;
}

const int controlInterface::get_finger_id(const string frame_id)
{ 
  int finger_id;
  if (frame_id.find("index") != std::string::npos) {
    finger_id = 0; 
  }
  else if (frame_id.find("middle") != std::string::npos) { 
    finger_id = 1;  
  }
  else if (frame_id.find("ring") != std::string::npos) { 
    finger_id = 2;
  }
  else if (frame_id.find("thumb") != std::string::npos) {
    finger_id = 3;
  }
  return finger_id;   
}

const string controlInterface::finger_id_to_biotac_tip_frame_name(const int finger_id)
{
  if (finger_id == 0) {
    return "index_biotac_tip";
  }
  if (finger_id == 1) {
    return "middle_biotac_tip";
  } 
  if (finger_id == 2) {
    return "ring_biotac_tip";
  } 
  if (finger_id == 3) {
    return "thumb_biotac_tip";
  } 
 
  log("Requesting frame name for unsupported finger.", ERROR);
  return "not-existing-frame";
}


}
}
