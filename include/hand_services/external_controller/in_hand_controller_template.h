#include <allegro_robot_interface/allegro_interface.h>
#include <ll4ma_robot_control/control.h>
#include <hand_services/ToggleController.h>
namespace hand_task_controller
{
class inHandController: public Controller
{
public:

  inHandController(std::string ns, robot_interface::RobotInterface *robot_interface,double loop_rate): Controller(ns,robot_interface, loop_rate){ }

  virtual bool configureHook();
  
  /** 
   * main control loop, this is defined. 
   * 
   **/
  
  virtual void updateHook();

protected:
  virtual bool get_control_cmd(Eigen::VectorXd &q,Eigen::VectorXd &qd)=0;

  std::vector<int> f_idx;
  
  /// service to toggle controller
  ros::ServiceServer control_toggle_req_;

  ros::ServiceServer update_control_req_;

  /// control toggled by service call #control_toggle_req_
  bool controller_enabled_;

  bool toggle_control(hand_services::ToggleController::Request &req,hand_services::ToggleController::Response &res);

  std::string toggle_srv_name_;
};
}
